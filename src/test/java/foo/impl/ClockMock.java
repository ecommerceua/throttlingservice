package foo.impl;

import foo.Clock;

public class ClockMock implements Clock {

    private long currentTime;

    @Override
    public long currentTimeMillis() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }
}
