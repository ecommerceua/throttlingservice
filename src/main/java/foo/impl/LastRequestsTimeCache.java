package foo.impl;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import foo.Clock;
import foo.SLA;

public class LastRequestsTimeCache {
  public static final int ONE_SECOND_IN_MS = 1000;

  private final Clock clock;
  Queue<Long> timestamps;

  LastRequestsTimeCache(SLA sla,Clock clock) {
    this.clock = clock;
    this.timestamps = new ArrayBlockingQueue<>(sla.getRps());
  }

  public boolean isRequestAllowed(){
    long currentTime = clock.currentTimeMillis();
    long minLimit = currentTime - ONE_SECOND_IN_MS;
    while (true) {
      Long t = timestamps.peek();
      if (t == null) {
        break;
      }
      if (minLimit > t) {
        timestamps.poll();
        if (timestamps.offer(currentTime)) {
          return true;
        }
      } else {
        break;
      }
    }
    return timestamps.offer(currentTime);
  }
}
