package foo;

import java.util.concurrent.CompletableFuture;

public interface SlaService {
  SLA getGuestSla();
  CompletableFuture<SLA> getSlaByToken(String token);
}
